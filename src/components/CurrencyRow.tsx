import { Box } from "@material-ui/core";
import React from "react";

type Props = {
  currencyOptions: string[];
  selectedCurrency: string;
  onChangeCurrency: React.ChangeEventHandler<HTMLSelectElement> | undefined;
  onChangeAmount:
    | React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>
    | undefined;
  amount: number;
};

export default function CurrencyRow({
  amount,
  currencyOptions,
  onChangeAmount,
  onChangeCurrency,
  selectedCurrency,
}: Props) {
  return (
    <Box component="div" display="inline">
      <input
        type="number"
        className="input"
        value={amount}
        onChange={onChangeAmount}
      />
      <select value={selectedCurrency} onChange={onChangeCurrency}>
        {currencyOptions.map((option) => (
          <option key={option} value={option}>
            {option}
          </option>
        ))}
      </select>
    </Box>
  );
}
