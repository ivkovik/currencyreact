import * as React from "react";
import { Button } from "@material-ui/core";
import { useEffect, useState } from "react";
import Timestamp from "./Timestamp";
import { SwapHorizontalCircle } from "@material-ui/icons";
import CurrencyRow from "./CurrencyRow";
import wellcome_img from "../assets/images/wellcome_img.jpg";

type Props = {
  firstCurrency: string;
  currencyOptions: string[];
  rates: number[];
  base: string;
  time: number;
};

function CurrencyExcange({
  currencyOptions,
  base,
  firstCurrency,
  rates,
  time,
}: Props) {
  const ratesValue: number[] = rates;
  const timestamp = time;
  const options: string[] = currencyOptions; //select keys
  const [fromCurrency, setFromCurrency] = useState<string>(base); //base currency or the value from the firs select
  const [toCurrency, setToCurrency] = useState<string>(firstCurrency); //to currency the selected currency
  const [exchangeRate, setExchangeRate] = useState<number>(
    rates[firstCurrency]
  ); //the rate of the toCurrency
  const [exchangeRateTo, setExchangeRateTo] = useState<number>(
    +(1 / rates[firstCurrency])
  ); //the rate od the fromCurrnecy
  //list of conversion rates
  const [amount, setAmount] = useState<number>(0);
  const [amountInFromCurrency, setAmountInFromCurrency] =
    useState<boolean>(true);

  let toAmount: number, fromAmount: number;
  if (amountInFromCurrency) {
    fromAmount = amount;
    toAmount = amount * exchangeRate;
  } else {
    toAmount = amount;
    fromAmount = amount / exchangeRate;
  }

  useEffect(() => {
    if (fromCurrency != null && toCurrency != null) {
      setExchangeRate(ratesValue[toCurrency]);
    }
  }, [fromCurrency, toCurrency, ratesValue]);

  function handleFromAmountChange(
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) {
    const x: number = +(e.target as HTMLInputElement).value;
    setAmount(x);
    setAmountInFromCurrency(true);
  }

  function handleToAmountChange(
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) {
    const x: number = +(e.target as HTMLInputElement).value;
    setAmount(x);
    setAmountInFromCurrency(false);
  }
  function swap(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    e.preventDefault();

    let currency = fromCurrency;
    let rate = exchangeRate;
    setFromCurrency(toCurrency);
    setToCurrency(currency);
    setExchangeRate(exchangeRateTo);
    setExchangeRateTo(rate);
  }

  return (
    <div className="top">
      <h1 color="primary">Convert</h1>
      <CurrencyRow
        currencyOptions={options}
        selectedCurrency={fromCurrency}
        onChangeCurrency={(e: React.ChangeEvent) => {
          const x = +(e.target as HTMLInputElement).value;
          setFromCurrency((e.target as HTMLInputElement).value);
          setExchangeRateTo(ratesValue[x]);
        }}
        onChangeAmount={handleFromAmountChange}
        amount={fromAmount}
      />
      <Button
        color="primary"
        startIcon={<SwapHorizontalCircle />}
        onClick={swap}
      >
        Swap
      </Button>
      <CurrencyRow
        currencyOptions={options}
        selectedCurrency={toCurrency}
        onChangeCurrency={(e: React.ChangeEvent) => {
          const x = (e.target as HTMLInputElement).value;

          setToCurrency(x);
          setExchangeRate(ratesValue[(e.target as HTMLInputElement).value]);
        }}
        onChangeAmount={handleToAmountChange}
        amount={toAmount}
      />{" "}
      <div>
        <p>
          1<strong>{fromCurrency}</strong>
          <strong> =</strong>
          {exchangeRate}
          <strong>{toCurrency}</strong>
        </p>
        <p>
          1 <strong>{toCurrency}</strong>
          <strong> =</strong>
          {exchangeRateTo}
          <strong>{fromCurrency}</strong>
        </p>
      </div>
      <img
        className="img1"
        src={wellcome_img}
        alt="money exchange illustration"
      />
      <Timestamp time={timestamp} />
    </div>
  );
}

export default CurrencyExcange;
