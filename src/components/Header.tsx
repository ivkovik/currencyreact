import * as React from "react";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import { Tabs, Tab, AppBar } from "@material-ui/core";

import CurrencyList from "../containers/CurrencyList";
import Wellcome from "../containers/Wellcome";
import FetchData from "src/containers/FetchData";

const Header = () => {
  return (
    <div>
      <BrowserRouter>
        <Route
          path="/"
          render={(history) => (
            <AppBar>
              <Tabs
                value={
                  history.location.pathname !== "/"
                    ? history.location.pathname
                    : false
                }
              >
                <Tab
                  label="Convert"
                  value="/convert"
                  component={Link}
                  to="/convert"
                />
                <Tab label="List" value="/list" component={Link} to="/list" />
              </Tabs>
            </AppBar>
          )}
        />

        <Switch>
          <Route path="/" exact component={Wellcome} />
          <Route path="/convert" component={FetchData} />
          <Route path="/list" component={CurrencyList} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default Header;
