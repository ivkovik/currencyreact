import * as React from "react";

import Moment from "react-moment";

interface TimeProp {
  time: number | undefined;
}

const Timestamp = ({ time }: TimeProp) => {
  // const unixTimestamp = {props.time};
  return <Moment unix>{time}</Moment>;
};

export default Timestamp;
