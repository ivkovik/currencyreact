import * as React from "react";

import wellcome_img from "../assets/images/wellcome_img.jpg";

const Wellcome = () => {
  return (
    <div className="top">
      <h1 className="wellcome ">Wellcome</h1>
      <img
        className="img"
        src={wellcome_img}
        alt="money exchange illustration"
      />
    </div>
  );
};

export default Wellcome;
