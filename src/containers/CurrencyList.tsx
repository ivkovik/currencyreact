import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Fetch from "react-fetch-component";
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const CurrencyList = () => {
  return (
    <div className="top">
      <TableContainer component={Paper}>
        <Table className="table" aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Currency</StyledTableCell>
              <StyledTableCell align="right">Rate</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <Fetch url="https://openexchangerates.org/api/latest.json?app_id=e6a2580aa98c4a6b87c8f45106632a51">
              {({ loading, data, error }) => (
                <>
                  {loading && console.log("loading data")}
                  {error && console.log(error)}
                  {data &&
                    Object.keys(data.rates).map((el) => (
                      <StyledTableRow key={el}>
                        <StyledTableCell align="right">{el}</StyledTableCell>
                        <StyledTableCell align="right">
                          {data.rates[el]}
                        </StyledTableCell>
                      </StyledTableRow>
                    ))}
                </>
              )}
            </Fetch>
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default CurrencyList;
