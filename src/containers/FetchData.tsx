import * as React from "react";
import Fetch from "react-fetch-component";
import CurrencyExcange from "src/components/CurrencyExcange";

const BASE_URL =
  "https://openexchangerates.org/api/latest.json?app_id=e6a2580aa98c4a6b87c8f45106632a51";

const FetchData = () => {
  return (
    <Fetch url={BASE_URL}>
      {({ loading, data, error }) => (
        <>
          {loading && <span>Loading...</span>}
          {error && console.log(error)}
          {data && (
            <CurrencyExcange
              firstCurrency={Object.keys(data.rates)[0]}
              currencyOptions={[...Object.keys(data.rates)]}
              rates={data.rates}
              base={data.base}
              time={data.timestamp}
            />
          )}
        </>
      )}
    </Fetch>
  );
};

export default FetchData;
